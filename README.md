# TD-Workbench

TD-Workbench是一个基于electron构建的，针对时序数据库TDengine的图形化管理工具。具有跨平台、易于使用、版本适应性强等特点。

> **说点什么** 由于TDengine官方未在开源版本提供客户端GUI工具，社区目前能找到的两款工具都不是很完善。本工具是在[TDengineGUI](https://gitee.com/skyebaobao/TDengineGUI)基础上改造而来，部分源码来自此处。


***还想说一句***

本人【 村口的大爷】，后端程序猿一枚，<color font="red">就爱写点代码，一杯茶一首歌，一个参数秀一天</color>

##  当前版本功能

> 通过TDengine restful接口连接到数据库，使用基本不受服务器版本升级影响

- 【连接配置】
	- [x] 新增
	- [x] 删除
	- [x] 修改
- 【数据库
	- [x] 添加数据库
	- [x] 删除数据库
	- [x] 显示数据库属性，并提供部分属性修改功能
- 【超表】
	- [x] 显示数据库下所有超表，分页显示并支持根据表名查询
	- [x] 创建超表
	- [x] 设计修改超表字段和标签
	- [x] 查询超表数据(默认降序显示，当数据行数大于1千万时自动切换默认的升序)
	- [x] 删除超表
	- [x] 提供基于超表创建子表
	- [x] 查看基于某张超表创建的所有子表
- 【普通表】
	- [x] 显示数据库下所有非基于超表创建的普通表，分页显示并支持根据表名查询
	- [x] 创建普通表
	- [x] 设计修改普通表的字段
	- [x] 查询普通的数据(默认降序显示，当数据行数大于1千万时自动切换默认的升序)
	- [x] 删除普通表
-  【自定义查询】
	- [x] 自由切换数据库
	- [x] 执行在textarea里编辑的sql
	- [x] 支持只执行选中的单条sql
	- [x] 结果集面板以列表形式显示并分页
	- [x] sql执行错误，错误日志显示在信息面板上

## 如何获取

### 下载最新版可执行文件：

[release v1.0.7](https://gitee.com/bhdweb/tdengine-workbench/releases/v1.0.7-release)



### 通过源代码运行开发版本：

```bash
# 首先全局安装nrm
npm i -g nrm
# 然后使用nrm切换为淘宝源，或者你已经切换了npm的源也是可以的，强烈不建议使用cnpm如果你不想看到什么奇奇怪怪的爆红问题
nrm ls
nrm use taobao
# 如果网络非常顺畅的情况下
npm install
# 如果网络出现一定的情况
# 建议不要使用cnpm，会出现各种玄学bug。您可以通过如下操作加快安装速度
npm install --registry=https://registry.npm.taobao.org
# 但是需要注意的是electron的本体下载并不是走这里所以还是要去设置一下
npm config edit
# 该命令会打开npm的配置文件，请在registry=https://registry.npm.taobao.org/下一行添加
# electron_mirror=https://cdn.npm.taobao.org/dist/electron/
# ELECTRON_BUILDER_BINARIES_MIRROR=http://npm.taobao.org/mirrors/electron-builder-binaries/
# 然后关闭该窗口，重启命令行，删除node_modules文件夹，并重新安装依赖即可
# 本地开发 clone代码
git clone https://gitee.com/bhdweb/tdengine-workbench
# 本地开发 启动
npm install
npm run dev 或 yarn dev
# 打包win 64位 安装包在 build目录下
npm run build:win64
```

## 使用说明

打开软件后，可以通过“新建连接”按钮，添加新的服务器进行管理。需要提供数据库服务器的ip地址、restful服务的端口号（默认为6041）、连接服务器的用户名和密码（默认为root:taosdata）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/221821_ac905458_70810.png "屏幕截图.png")

连接成功后，点连接前![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/222009_141f9b36_70810.png "屏幕截图.png")这个小图标就可看到下面的界面了，页面左侧第一级是数据库连接，第二级是数据库，第三是超表和普通

![输入图片说明](https://images.gitee.com/uploads/images/2021/0714/222151_b4feeded_70810.png "屏幕截图.png")

其它具体操作，各位自已慢慢摸索吧~~~~

