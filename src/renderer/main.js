import Vue from 'vue'

import App from './App'
import store from './store'
// 引用element
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss'

if (!process.env.IS_WEB) {
  // 当处于electron状态下才引用db
  Vue.prototype.$ipcApi = require('./utils/ipcRenderer').default
}

Vue.use(ElementUI)

Vue.directive('focus', function (el, option) {
  var defClass = 'el-input', defTag = 'input';
  var value = option.value || true;
  if (typeof value === 'boolean')
    value = { cls: defClass, tag: defTag, foc: value };
  else
    value = { cls: value.cls || defClass, tag: value.tag || defTag, foc: value.foc || false };
  if (el.classList.contains(value.cls) && value.foc)
    el.getElementsByTagName(value.tag)[0].focus();
});

Vue.config.productionTip = false
/* eslint-disable no-new */
const vue = new Vue({
  components: { App },
  store,
  template: '<App/>'
}).$mount('#app')

export default vue
