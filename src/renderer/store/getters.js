/**
 * 在这里则是state.文件名.状态名
 */
const getters = {
  link: state => state.user.link,
  curObject: state => state.user.curObject
}
export default getters
