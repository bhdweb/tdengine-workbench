const user = {
  state: {
	curObject: {},
	link: JSON.parse(localStorage.getItem("link"))?JSON.parse(localStorage.getItem("link")):{}
  },

  mutations: {
	  SET_LINK: (state, link) => {
		localStorage.setItem('link', JSON.stringify(link))
		state.link = link
	  },
	  SET_CUROBJECT: (state, curObject) => {
		state.curObject = curObject
	  }
  },

  actions: {
	//添加数据数据连接
	addDbLink({
	  commit
	}, data) {
	  return new Promise((resolve, reject) => {
	    commit('SET_LINK', data)
	    resolve()
	  })
	},
	//切换左侧选中的对象
	changeCurObject({
	  commit
	}, data) {
	  return new Promise((resolve, reject) => {
	    commit('SET_CUROBJECT', data)
	    resolve()
	  })
	}
  }
}

export default user
